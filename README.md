# DocflowExtensions. Доработка конфигурации Документооборот государственного учреждения, редакция 2.1 (2.1.26.2). Платформа  8.3.17.1549.

## ВыводПолнойИстории
- Общие модули, ПомощникОтправить. Группировка процессов при отправке на рекомендуемые, стандартные, общие. Использовалась [Статья на Инфостарте](https://infostart.ru/1c/articles/1446957/);
- Общие модули, ОбзорЗадачВызовСервераПереопределяемый. Вывод полной истории в задаче по всему "дереву" бизнес-процессов. Использовалась [Статья на Инфостарте](https://infostart.ru/1c/articles/1156346/).
____

## ВыводСпискаПроцессов
 - [ ] Пустое расширение. Вынести группировку процессов из расширения "ВыводПолнойИстории".
____

## Проекты
- Справочник Проекты, ФормаЭлемента, МодульФормы:
    - Убрана доступность кода и индекса нумерации;
    - Индекс нумерации присваивается при записи, равняется коду без нулей.
- Обработки:
    - ОтчетДоговоры. Пользователь выбирает вид документа (из видов договоров), состояние документа, начало и конец периода создания документов, выводить или нет визы согласования.	
____

## УбратьСоглЗамечаниями
- Отключена видимость кнопки "Согласовать с замечаниями" в задачах на согласование:
    - Бизнес-процессы, Согласование, ФормаЗадачиИсполнителя, МодульФормы;
    - Задачи, ЗадачаИсполнителя, ЗадачиМне, МодульФормы.
____

## ЭЦПРаа
- Общие модули, РаботаСКартинками. При большом количестве символов в представлении ЭЦП шрифт на штампе ЭЦП устанавливается меньше (```СформироватьШтампЭП(ОписаниеЭП, Формат)```);
- Общие формы, СохранениеВместеСЭлектроннойПодписью, МодульФормы. При сохранении подписи подписанного файла устанавливается название файла подписи длиной в 35 символов.